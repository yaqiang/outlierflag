/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outlierflag;

import java.util.ArrayList;
import java.util.List;
import ucar.ma2.Array;
import ucar.ma2.DataType;

/**
 *
 * @author huangshuai
 */
public class FlagCheck {

    /**
     * Check limitation
     *
     * @param data Input data
     * @param asetting QASetting
     * @return Check code list
     */
    public static List<String> check_Limitation_Data(List<Double> data, FlagSetting asetting) {
        double value;
        String aVCode;
        int dn = data.size();
        List<String> codeList = new ArrayList<>();

        for (int i = 0; i < dn; i++) {
            aVCode = "V0";
            value = data.get(i);
            if (value < asetting.getMinLimination() || value > asetting.getMaxLimitation()) {
                aVCode = "V2";
            }

            codeList.add(aVCode);
        }

        return codeList;
    }

    /**
     * Check limitation
     *
     * @param data Input data
     * @param asetting QASetting
     * @return Check code list
     */
    public static List<String> check_Limitation(List<String> data, FlagSetting asetting) {
        double value;
        String aVCode, dstr;
        int dn = data.size();
        List<String> codeList = new ArrayList<>();

        for (int i = 0; i < dn; i++) {
            aVCode = "V0";
            dstr = data.get(i);
            try {
                value = Double.parseDouble(dstr);
                if (value < asetting.getMinLimination() || value > asetting.getMaxLimitation()) {
                    aVCode = "V2";
                }
            } catch (Exception e) {
                aVCode = "V1";
            }

            codeList.add(aVCode);
        }

        return codeList;
    }

    /**
     * Check limitation
     *
     * @param data Input data
     * @param asetting QASetting
     * @return Check code list
     */
    public static List<String> check_Limitation(Array data, FlagSetting asetting) {
        double value;
        String aVCode;
        int dn = (int) data.getSize();
        List<String> codeList = new ArrayList<>();

        for (int i = 0; i < dn; i++) {
            aVCode = "V0";
            value = data.getDouble(i);
            if (value < asetting.getMinLimination() || value > asetting.getMaxLimitation()) {
                aVCode = "V2";
            }

            codeList.add(aVCode);
        }

        return codeList;
    }

    /**
     * Check error
     *
     * @param data Input data
     * @param aSetting QASetting
     * @param codeList Input code list
     */
    public static void check_Error_data(List<Double> data, FlagSetting aSetting, List<String> codeList) {
        int halfNum = (aSetting.getErrorPNum() - 1) / 2;
        double value;
        String aVCode;
        int dn = data.size();

        //Calculate error data list
        List<double[]> errorList = new ArrayList<>();
        for (int i = 0; i < dn; i++) {
            aVCode = codeList.get(i);
            if (!aVCode.substring(1).equals("0")) {
                continue;
            }

            value = data.get(i);
            double aSum = value;
            double bValue;
            int idx = i - 1;
            int j = 1;
            int dNum = 1;
            while (j <= halfNum) {
                if (idx >= 0) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        bValue = data.get(idx);
                        aSum += bValue;
                        dNum += 1;
                        j += 1;
                    }
                    idx -= 1;
                } else {
                    break;
                }
            }

            idx = i + 1;
            j = 1;
            while (j <= halfNum) {
                if (idx < dn) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        bValue = data.get(idx);
                        aSum += bValue;
                        dNum += 1;
                        j += 1;
                    }
                    idx += 1;
                } else {
                    break;
                }
            }

            if (dNum > 0) {
                double ave = aSum / dNum;
                double error = Math.abs(value - ave);
                errorList.add(new double[]{i, error});
            }
        }

        if (errorList.size() < 10) {
            return;
        }

        //Check error data list
        int halfQNum = (aSetting.getQuantilePNum() - 1) / 2;
        for (int i = 0; i < errorList.size(); i++) {
            List<Double> quantileList = new ArrayList<>();
            quantileList.add(errorList.get(i)[1]);
            int j = 1;
            int idx = i - 1;
            while (j <= halfQNum) {
                if (idx >= 0) {
                    quantileList.add(errorList.get(idx)[1]);
                    j += 1;
                    idx -= 1;
                } else {
                    break;
                }
            }

            j = 1;
            idx = i + 1;
            while (j <= halfQNum) {
                if (idx < errorList.size()) {
                    quantileList.add(errorList.get(idx)[1]);
                    j += 1;
                    idx += 1;
                } else {
                    break;
                }
            }

            double qValue = Statistics.quantile(quantileList, aSetting.getQuantile());
            if (errorList.get(i)[1] > qValue * aSetting.getFactor()) {
                aVCode = "V3";
                codeList.set((int) errorList.get(i)[0], aVCode);
            }
        }
    }

    /**
     * Check error
     *
     * @param data Input data
     * @param aSetting QASetting
     * @param codeList Input code list
     */
    public static void check_Error(Array data, FlagSetting aSetting, List<String> codeList) {
        int halfNum = (aSetting.getErrorPNum() - 1) / 2;
        double value;
        String aVCode;
        int dn = (int) data.getSize();

        //Calculate error data list
        List<double[]> errorList = new ArrayList<>();
        for (int i = 0; i < dn; i++) {
            aVCode = codeList.get(i);
            if (!aVCode.substring(1).equals("0")) {
                continue;
            }

            value = data.getDouble(i);
            double aSum = value;
            double bValue;
            int idx = i - 1;
            int j = 1;
            int dNum = 1;
            while (j <= halfNum) {
                if (idx >= 0) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        bValue = data.getDouble(idx);
                        aSum += bValue;
                        dNum += 1;
                        j += 1;
                    }
                    idx -= 1;
                } else {
                    break;
                }
            }

            idx = i + 1;
            j = 1;
            while (j <= halfNum) {
                if (idx < dn) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        bValue = data.getDouble(idx);
                        aSum += bValue;
                        dNum += 1;
                        j += 1;
                    }
                    idx += 1;
                } else {
                    break;
                }
            }

            if (dNum > 0) {
                double ave = aSum / dNum;
                double error = Math.abs(value - ave);
                errorList.add(new double[]{i, error});
            }
        }

        if (errorList.size() < 10) {
            return;
        }

        //Check error data list
        int halfQNum = (aSetting.getQuantilePNum() - 1) / 2;
        for (int i = 0; i < errorList.size(); i++) {
            List<Double> quantileList = new ArrayList<>();
            quantileList.add(errorList.get(i)[1]);
            int j = 1;
            int idx = i - 1;
            while (j <= halfQNum) {
                if (idx >= 0) {
                    quantileList.add(errorList.get(idx)[1]);
                    j += 1;
                    idx -= 1;
                } else {
                    break;
                }
            }

            j = 1;
            idx = i + 1;
            while (j <= halfQNum) {
                if (idx < errorList.size()) {
                    quantileList.add(errorList.get(idx)[1]);
                    j += 1;
                    idx += 1;
                } else {
                    break;
                }
            }

            double qValue = Statistics.quantile(quantileList, aSetting.getQuantile());
            if (errorList.get(i)[1] > qValue * aSetting.getFactor()) {
                aVCode = "V3";
                codeList.set((int) errorList.get(i)[0], aVCode);
            }
        }
    }

    public static void check_Error(List<String> data, FlagSetting aSetting, List<String> codeList) {
        int halfNum = (aSetting.getErrorPNum() - 1) / 2;
        double value;
        String aVCode, dstr, dstring;
        int dn = data.size();

        //Calculate error data list
        List<Double[]> errorList = new ArrayList<>();
        for (int i = 0; i < dn; i++) {

            aVCode = codeList.get(i);
            if (!aVCode.substring(1).equals("0")) {
                continue;
            }

            dstr = data.get(i);
            value = Double.parseDouble(dstr);
            double aSum = value;
            double bValue;
            int idx = i - 1;
            int j = 1;
            int dNum = 1;
            while (j <= halfNum) {
                if (idx >= 0) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        dstring = data.get(idx);
                        bValue = Double.parseDouble(dstring);
                        aSum += bValue;
                        dNum += 1;
                        j += 1;
                    }
                    idx -= 1;
                } else {
                    break;
                }
            }

            idx = i + 1;
            j = 1;
            while (j <= halfNum) {
                if (idx < dn) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        dstring = data.get(idx);
                        bValue = Double.parseDouble(dstring);
                        aSum += bValue;
                        dNum += 1;
                        j += 1;
                    }
                    idx += 1;
                } else {
                    break;
                }
            }

            if (dNum > 0) {
                double ave = aSum / dNum;
                double error = Math.abs(value - ave);
                errorList.add(new Double[]{(double) i, error});
            }
        }

        //Check error data list
        int halfQNum = (aSetting.getQuantilePNum() - 1) / 2;
        for (int i = 0; i < errorList.size(); i++) {
            List<Double> quantileList = new ArrayList<>();
            quantileList.add(errorList.get(i)[1]);
            int j = 1;
            int idx = i - 1;
            while (j <= halfQNum) {
                if (idx >= 0) {
                    quantileList.add(errorList.get(idx)[1]);
                    j += 1;
                    idx -= 1;
                } else {
                    break;
                }
            }

            j = 1;
            idx = i + 1;
            while (j <= halfQNum) {
                if (idx < errorList.size()) {
                    quantileList.add(errorList.get(idx)[1]);
                    j += 1;
                    idx += 1;
                } else {
                    break;
                }
            }

            double qValue = Statistics.quantile(quantileList, aSetting.getQuantile());
            if (errorList.get(i)[1] > qValue * aSetting.getFactor()) {
                aVCode = "V3";
                codeList.set(errorList.get(i)[0].intValue(), aVCode);
            }
        }
    }

    public static void check_StandardDeviation_data(List<Double> data, FlagSetting aSetting, List<String> codeList) {
        int halfNum = (aSetting.getSDPNum() - 1) / 2;
        double value;
        String aVCode;
        int dn = data.size();

        //Calculate sdandard deviation data list
        for (int i = 0; i < dn; i++) {
            aVCode = codeList.get(i);
            if (!aVCode.substring(1).equals("0")) {
                continue;
            }

            value = data.get(i);
            List<Double> valueList = new ArrayList<>();
            valueList.add(value);
            double bValue;
            int idx = i - 1;
            int j = 1;
            while (j <= halfNum) {
                if (idx >= 0) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        bValue = data.get(idx);
                        valueList.add(bValue);
                        j += 1;
                    }
                    idx -= 1;
                } else {
                    break;
                }
            }

            idx = i + 1;
            j = 1;
            while (j <= halfNum) {
                if (idx < dn) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        bValue = data.get(idx);
                        valueList.add(bValue);
                        j += 1;
                    }
                    idx += 1;
                } else {
                    break;
                }
            }

            if (valueList.size() > 0) {
                double ave = Statistics.mean(valueList);
                double error = Math.abs(value - ave);
                double sd = Statistics.StandardDeviation(valueList);
                if (error > sd * aSetting.getSDFactor()) {
                    aVCode = "V4";
                    codeList.set(i, aVCode);
                }
            }
        }
        //System.out.println(codeList);
    }

    /**
     * Check 3 times of standard deviation
     *
     * @param data Input data
     * @param aSetting Flag setting
     * @param codeList Input code list
     */
    public static void check_StandardDeviation(Array data, FlagSetting aSetting, List<String> codeList) {
        int halfNum = (aSetting.getSDPNum() - 1) / 2;
        double value;
        String aVCode;
        int dn = (int) data.getSize();

        //Calculate sdandard deviation data list
        for (int i = 0; i < dn; i++) {
            aVCode = codeList.get(i);
            if (!aVCode.substring(1).equals("0")) {
                continue;
            }

            value = data.getDouble(i);
            List<Double> valueList = new ArrayList<>();
            valueList.add(value);
            double bValue;
            int idx = i - 1;
            int j = 1;
            while (j <= halfNum) {
                if (idx >= 0) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        bValue = data.getDouble(idx);
                        valueList.add(bValue);
                        j += 1;
                    }
                    idx -= 1;
                } else {
                    break;
                }
            }

            idx = i + 1;
            j = 1;
            while (j <= halfNum) {
                if (idx < dn) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        bValue = data.getDouble(idx);
                        valueList.add(bValue);
                        j += 1;
                    }
                    idx += 1;
                } else {
                    break;
                }
            }

            if (valueList.size() > 0) {
                double ave = Statistics.mean(valueList);
                double error = Math.abs(value - ave);
                double sd = Statistics.StandardDeviation(valueList);
                if (error > sd * aSetting.getSDFactor()) {
                    aVCode = "V4";
                    codeList.set(i, aVCode);
                }
            }
        }
        //System.out.println(codeList);
    }

    /**
     *
     * @param data Input data
     * @param aSetting QASetting
     * @param codeList Input code list
     */
    public static void check_StandardDeviation(List<String> data, FlagSetting aSetting, List<String> codeList) {
        int halfNum = (aSetting.getSDPNum() - 1) / 2;
        double value;
        String aVCode, dstr, dstring;
        int dn = data.size();

        //Calculate sdandard deviation data list
        for (int i = 0; i < dn; i++) {
            aVCode = codeList.get(i);
            if (!aVCode.substring(1).equals("0") || i == 0) {
                continue;
            }

            dstr = data.get(i);
            value = Double.parseDouble(dstr);
            List<Double> valueList = new ArrayList<>();
            valueList.add(value);
            double bValue;
            int idx = i - 1;
            int j = 1;
            while (j <= halfNum) {
                if (idx >= 0) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        dstring = data.get(idx);
                        bValue = Double.parseDouble(dstring);
                        valueList.add(bValue);
                        j += 1;
                    }
                    idx -= 1;
                } else {
                    break;
                }
            }

            idx = i + 1;
            j = 1;
            while (j <= halfNum) {
                if (idx < dn) {
                    if (codeList.get(idx).substring(1).equals("0")) {
                        dstring = data.get(idx);
                        bValue = Double.parseDouble(dstring);
                        valueList.add(bValue);
                        j += 1;
                    }
                    idx += 1;
                } else {
                    break;
                }
            }

            if (valueList.size() > 0) {
                double ave = Statistics.mean((valueList));
                double error = Math.abs(value - ave);
                double sd = Statistics.StandardDeviation(valueList);
                if (error > sd * aSetting.getSDFactor()) {
                    aVCode = "V4";
                    codeList.set(i, aVCode);
                }
            }
        }
    }

    /**
     * Black carbon loading effect compensation
     * @param bcArray Input BC data array
     * @param atnArray Input Attanuation data array
     * @param aveNum Average number
     * @param isRefine Is refine or not
     * @param isSmooth Is smooth or not
     * @param smoothNum Smooth number
     * @return Compensated BC data array and compensation parameters
     */
    public static Object[] bc_loading_effect(Array bcArray, Array atnArray, int aveNum, boolean isRefine,
            boolean isSmooth, int smoothNum) {
        //---- Correct attenuation value
        int i;
        int j;
        double aATN;
        double oATN = 0;
        int[] shape = bcArray.getShape();
        int waveNum;
        if (shape.length == 1) {
            waveNum = 1;
        } else {
            waveNum = shape[0];
        }
        int dataNum = shape[shape.length - 1];

        //---- Identify tape advances
        int blankNum = 0;
        //int[,] advIDArray = new int[2, -1 + 1];
        List<int[]> advIDArray = new ArrayList<>();

        int advNum = 0;
        for (i = 0; i < dataNum; i++) {
            aATN = atnArray.getDouble(i);
            if (i == 0) {
                oATN = aATN;
            }
            if (aATN == 0) {
                blankNum += 1;
            } else if (blankNum > 0) {
                if (advNum == 0) {
                    if (i - blankNum - 1 == 0 && oATN == 0) {
                        blankNum = 0;
                        continue;
                    }
                }
                if (blankNum > 2 && blankNum < 5) {
                    if (aATN < 30) {
                        if (atnArray.getDouble(i) < atnArray.getDouble(i - blankNum - 1)) {
                            advNum += 1;
                            advIDArray.add(new int[]{i - blankNum - 1, i});
                        }
                    }
                }
                blankNum = 0;
            }
        }

        //---- Calculate the data compensation parameter
        double[][] BC1 = new double[waveNum][advNum];
        double[][] BC2 = new double[waveNum][advNum];
        double[][] A1 = new double[waveNum][advNum];
        double[][] A2 = new double[waveNum][advNum];
        List<Double> aDataList;
        int r;
        Array fv = Array.factory(DataType.DOUBLE, new int[]{waveNum, advNum});
        for (i = 0; i < waveNum; i++) {
            for (j = 0; j < advNum; j++) {
                if (j == 0) {
                    if (advIDArray.get(j)[0] < aveNum - 1) {
                        fv.setDouble(i * advNum + j, 0);
                        continue;
                    }
                }
                if (j == advNum - 1) {
                    if (advIDArray.get(j)[1] > advNum - aveNum - 2) {
                        fv.setDouble(i * advNum + j, 0);
                        continue;
                    }
                }

                aDataList = new ArrayList<>();
                for (r = 0; r < aveNum; r++) {
                    aDataList.add(bcArray.getDouble(i * dataNum + advIDArray.get(j)[0] - r));
                }
                BC1[i][j] = Statistics.mean(aDataList);
                aDataList.clear();
                for (r = 0; r < aveNum; r++) {
                    aDataList.add(bcArray.getDouble(i * dataNum + advIDArray.get(j)[1] + r));
                }
                BC2[i][j] = Statistics.mean(aDataList);
                aDataList.clear();
                for (r = 0; r < aveNum; r++) {
                    aDataList.add(atnArray.getDouble(i * dataNum + advIDArray.get(j)[0] - r));
                }
                A1[i][j] = Statistics.mean(aDataList);
                aDataList.clear();
                for (r = 0; r < aveNum; r++) {
                    aDataList.add(atnArray.getDouble(i * dataNum + advIDArray.get(j)[1] + r));
                }
                A2[i][j] = Statistics.mean(aDataList);
                fv.setDouble(i * advNum + j, (BC2[i][j] - BC1[i][j]) / (BC1[i][j] * A1[i][j] - BC2[i][j] * A2[i][j]));
            }
        }

        //---- Refine the time series of compensation parameters
        if (isRefine) {
            double Q1;
            double Q3;
            double IQR;

            for (i = 0; i < waveNum; i++) {
                aDataList = new ArrayList<>();
                for (j = 0; j < advNum; j++) {
                    aDataList.add(fv.getDouble(i * advNum + j));
                }
                Q1 = Statistics.quantile(aDataList, 1);
                Q3 = Statistics.quantile(aDataList, 3);
                IQR = Q3 - Q1;
                for (j = 0; j < advNum; j++) {
                    if (fv.getDouble(i * advNum + j) > Q3 + 3 * IQR) {
                        fv.setDouble(i * advNum + j, Q3 + 3 * IQR);
                    }
                    if (fv.getDouble(i * advNum + j) < Q1 - 3 * IQR) {
                        fv.setDouble(i * advNum + j, Q1 - 3 * IQR);
                    }
                }
            }
        }

        //---- Smooth the time series for the compensation parameter
        Array sfv = null;
        if (isSmooth) {
            int halfNum;
            sfv = Array.factory(DataType.DOUBLE, new int[]{waveNum, advNum});

            halfNum = (int) Math.floor((double) smoothNum / 2);
            for (i = 0; i < waveNum; i++) {
                for (j = 0; j < advNum; j++) {
                    if (j >= halfNum && j <= advNum - halfNum - 1) {
                        aDataList = new ArrayList<>();
                        for (r = 0; r <= smoothNum; r++) {
                            aDataList.add(fv.getDouble(i * advNum + j - halfNum + r));
                        }
                        sfv.setDouble(i * advNum + j, Statistics.mean(aDataList));
                    }
                }
            }
            for (i = 0; i < waveNum; i++) {
                for (j = 0; j < advNum; j++) {
                    if (j < halfNum) {
                        sfv.setDouble(i * advNum + j, sfv.getDouble(i * advNum + halfNum));
                    }
                    if (j > advNum - halfNum - 1) {
                        sfv.setDouble(i * advNum + j, sfv.getDouble(i * advNum + advNum - halfNum - 1));
                    }
                }
            }
        }

        //---- Apply the smoothed compensation parameter time series to the concentration data
        Array nBCArray = Array.factory(DataType.DOUBLE, shape);
        for (i = 0; i < waveNum; i++) {
            for (j = 0; j < dataNum; j++) {
                for (r = 0; r < advNum; r++) {
                    if (j <= advIDArray.get(r)[0]) {
                        if (atnArray.getDouble(i * dataNum + j) != 0) {
                            if (isSmooth) {
                                nBCArray.setDouble(i * dataNum + j, (bcArray.getDouble(i * dataNum + j) * (1 + sfv.getDouble(i * advNum + r) * atnArray.getDouble(i * dataNum + j))));
                            } else {
                                nBCArray.setDouble(i * dataNum + j, (bcArray.getDouble(i * dataNum + j) * (1 + fv.getDouble(i * advNum + r) * atnArray.getDouble(i * dataNum + j))));
                            }
                        } else {
                            nBCArray.setDouble(i * dataNum + j, 0);
                        }
                        break;
                    }
                    if (j >= advIDArray.get(advNum - 1)[1]) {
                        nBCArray.setDouble(i * dataNum + j, bcArray.getDouble(i * dataNum + j));
                    }
                }
            }
            //parent.getProgressBar().setValue(i * 100 / (waveNum - 1));            
        }
        if (isSmooth) {
            return new Object[]{nBCArray, sfv};
        } else {
            return new Object[]{nBCArray, fv};
        }
    }
}
