/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package outlierflag;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author huangshuai
 */
public class FlagSetting {
    // <editor-fold desc="Variables">
    private double _maxLimitation;
    private double _minLimitation;
    private int _errorPNum;
    private int _quantilePNum;
    private double _quantile;
    private double _factor;
    private int _sdPNum;
    private double _sdFactor;
    // </editor-fold>

    // <editor-fold desc="Constructor">
    /**
     * Constructor
     */    
    public FlagSetting() {
        _maxLimitation = 50000;
        _minLimitation = -10000;
        _errorPNum = 11;
        _quantilePNum = 21;
        _quantile = 0.9;
        _factor = 2.3;
        _sdPNum = 29;
        _sdFactor = 3;
    }
    
    /**
     * Constructor
     * @param minlim
     * @param maxlim
     * @param errornum
     * @param quantilenum
     * @param quantile
     * @param factor
     * @param stdnum
     * @param stdfactor 
     */
    public FlagSetting(double minlim, double maxlim, int errornum, int quantilenum, double quantile,
            double factor, int stdnum, double stdfactor){
        this._minLimitation = minlim;
        this._maxLimitation = maxlim;
        this._errorPNum = errornum;
        this._quantilePNum = quantilenum;
        this._quantile = quantile;
        this._factor = factor;
        this._sdPNum = stdnum;
        this._sdFactor = stdfactor;
    }
    // </editor-fold>

    // <editor-fold desc="Get set">
    /**
     * Get maximum limitation value
     *
     * @return Maximum limitation value
     */
    public  double getMaxLimitation() {
        return _maxLimitation;
    }

    /**
     * Set maximum limitation value
     *
     * @param value Maximum limitation value
     */
    public void setMaxLimitation(double value) {
        _maxLimitation = value;
    }

    /**
     * Get minimum limitation value
     *
     * @return Minimum limitition value
     */
    public double getMinLimination() {
        return _minLimitation;
    }

    /**
     * Set minimum limitation value
     *
     * @param value Minimum limitation value
     */
    public void setMinLimitation(double value) {
        _minLimitation = value;
    }

    /**
     * Get error point number value
     *
     * @return Error point number value
     */
    public int getErrorPNum() {
        return _errorPNum;
    }

    /**
     * Set error point number value
     *
     * @param value Ereoe point number value
     */
    public void setErrorPNum(int value) {
        _errorPNum = value;
    }

    /**
     * Get average point number value
     *
     * @return Average point number value
     */
    public int getQuantilePNum() {
        return _quantilePNum;
    }

    /**
     * Set average point number value
     *
     * @param value Average point number value
     */
    public void setQuantilePNum(int value) {
        _quantilePNum = value;
    }

    /**
     * Get quantile value
     *
     * @return Quantile value
     */
    public double getQuantile() {
        return _quantile;
    }

    /**
     * Set quantile value
     *
     * @param value Quantile value
     */
    public void setQuantile(double value) {
        _quantile = value;
    }

    /**
     * Get factor value
     *
     * @return Factor value
     */
    public double getFactor() {
        return _factor;
    }

    /**
     * Set factor value
     *
     * @param value Factor value
     */
    public void setFactor(double value) {
        _factor = value;
    }

    /**
     * Get standard deviation point number value
     *
     * @return Standard deviation point number value
     */
    public int getSDPNum() {
        return _sdPNum;
    }

    /**
     * Set standard deviation point number value
     *
     * @param value Standard deviation point number value
     */
    public void setSDPNum(int value) {
        _sdPNum = value;
    }

    /**
     * Get standard deviation factor value
     *
     * @return Standard deviation factor value
     */
    public double getSDFactor() {
        return _sdFactor;
    }

    /**
     * Set standard deviation factor value
     *
     * @param value Standard deviation factor value
     */
    public void setSDFactor(double value) {
        _sdFactor = value;
    }
    
    // </editor-fold>
    
    /**
     * Export to xml document
     *
     * @param doc xml document
     * @param parent Parent xml element
     */
    public void exportToXML(Document doc, Element parent) {
        Element root = doc.createElement("FlagSetting");
        Attr maxAttr = doc.createAttribute("MaxLimitation");
        Attr minAttr = doc.createAttribute("MinLimitation");
        Attr erroPNumAttr = doc.createAttribute("ErrorPointNumber");
        Attr quantilePNumAttr = doc.createAttribute("QuantilePointNumber");
        Attr quantileAttr = doc.createAttribute("Quantile");
        Attr quantileFactorAttr = doc.createAttribute("QuantileFactor");
        Attr sdPNumAttr = doc.createAttribute("StDevPointNumber");
        Attr sdFactorAttr = doc.createAttribute("StDevFactor");
        
        maxAttr.setValue(String.valueOf(this._maxLimitation));
        minAttr.setValue(String.valueOf(this._minLimitation));
        erroPNumAttr.setValue(String.valueOf(this._errorPNum));
        quantilePNumAttr.setValue(String.valueOf(this._quantilePNum));
        quantileAttr.setValue(String.valueOf(this._quantile));
        quantileFactorAttr.setValue(String.valueOf(this._factor));
        sdPNumAttr.setValue(String.valueOf(this._sdPNum));
        sdFactorAttr.setValue(String.valueOf(this._sdFactor));
        
        root.setAttributeNode(maxAttr);
        root.setAttributeNode(minAttr);
        root.setAttributeNode(erroPNumAttr);
        root.setAttributeNode(quantilePNumAttr);
        root.setAttributeNode(quantileAttr);
        root.setAttributeNode(quantileFactorAttr);
        root.setAttributeNode(sdPNumAttr);
        root.setAttributeNode(sdFactorAttr);
        
        parent.appendChild(root);
    }
    
    /**
     * Export to xml file
     *
     * @param aFile xml file path
     * @throws ParserConfigurationException
     */
    public void exportToXMLFile(String aFile) throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        Element root = doc.createElement("OutlierFlag");
        doc.appendChild(root);

        exportToXML(doc, root);

        //Save to file
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            DOMSource source = new DOMSource(doc);
            
            Properties properties = transformer.getOutputProperties();
            properties.setProperty(OutputKeys.ENCODING, "UTF-8");
            properties.setProperty(OutputKeys.INDENT, "yes");
            properties.setProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.setOutputProperties(properties);
            FileOutputStream out = new FileOutputStream(aFile);
            StreamResult result = new StreamResult(out);
            transformer.transform(source, result);
        } catch (TransformerException | IOException mye) {
        }
    }
    
    /**
     * Import legend scheme from xml node
     *
     * @param flagNode xml node
     */
    public void importFromXML(Node flagNode) {
        try {
            this._minLimitation = Double.parseDouble(flagNode.getAttributes().getNamedItem("MinLimitation").getNodeValue());
            this._maxLimitation = Double.parseDouble(flagNode.getAttributes().getNamedItem("MaxLimitation").getNodeValue());
            this._errorPNum = Integer.parseInt(flagNode.getAttributes().getNamedItem("ErrorPointNumber").getNodeValue());
            this._quantilePNum = Integer.parseInt(flagNode.getAttributes().getNamedItem("QuantilePointNumber").getNodeValue());
            this._quantile = Double.parseDouble(flagNode.getAttributes().getNamedItem("Quantile").getNodeValue());
            this._factor = Double.parseDouble(flagNode.getAttributes().getNamedItem("QuantileFactor").getNodeValue());
            this._sdPNum = Integer.parseInt(flagNode.getAttributes().getNamedItem("StDevPointNumber").getNodeValue());
            this._sdFactor = Double.parseDouble(flagNode.getAttributes().getNamedItem("StDevFactor").getNodeValue());
        } catch (Exception e){
            
        }
    }
    
    /**
     * Import legend scheme from XML file
     *
     * @param aFile file path
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void importFromXMLFile(String aFile) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new File(aFile));

        Element root = doc.getDocumentElement();
        Node flagNode;
        if ("OutlierFlag".equals(root.getNodeName())) {
            flagNode = root.getElementsByTagName("FlagSetting").item(0);
        } else {
            flagNode = root;
        }

        importFromXML(flagNode);
    }
}


