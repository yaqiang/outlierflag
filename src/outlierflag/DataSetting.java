/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outlierflag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author yaqiang
 */
public class DataSetting {
    // <editor-fold desc="Variables">
    private List<String> dataColumnNames;
    private boolean timeData;
    private String timeColumnName;
    private String dateFormat;
    private int timeDeltaValue;
    private String timeDeltaType;
    private int aimcolnum;
    private List<Integer> aimcolnums;
    // </editor-fold>
    // <editor-fold desc="Constructor">
    /**
     * Constructor
     */
    public DataSetting(){
        this.dataColumnNames = new ArrayList<>();
        this.timeData = false;
        this.timeColumnName = "Time";
        this.dateFormat = "yyyyMMddHHmm";
        this.timeDeltaValue = 5;
        this.timeDeltaType = "MINUTE";
    }
    // </editor-fold>
    // <editor-fold desc="Get Set Methods">
    /**
     * Get if is time series data
     * @return Boolean
     */
    public boolean isTimeData(){
        return this.timeData;
    }
    
    /**
     * Set if is time series data
     * @param value Boolean
     */
    public void setTimeData(boolean value){
        this.timeData = value;
    }
    
    /**
     * Get time column name
     * @return Time column name
     */
    public String getTimeColumnName(){
        return this.timeColumnName;
    }
    
    /**
     * Set time column name
     * @param value Time column name
     */
    public void setTimeColumnName(String value){
        this.timeColumnName = value;
    }
    
    /**
     * Get data column names
     * @return Data column names
     */
    public List<String> getDataColumnNames(){
        return this.dataColumnNames;
    }
    
    /**
     * Set data column names
     * @param value Data column names
     */
    public void setDataColumnNames(List<String> value){
        this.dataColumnNames = value;
    }
    
    /**
     * Get date format string
     * @return Date format string
     */
    public String getDateFormat(){
        return this.dateFormat;
    }
    
    /**
     * Set date format
     * @param value Date format
     */
    public void setDateFormat(String value){
        this.dateFormat = value;
    }
    
    /**
     * Get time delta value
     * @return Time delta value
     */
    public int getTimeDeltaValue(){
        return this.timeDeltaValue;
    }
    
    /**
     * Set time delta value
     * @param value Time delta value
     */
    public void setTimeDeltaValue(int value){
        this.timeDeltaValue = value;
    }
    
    /**
     * Get aim column number value
     * @return Aim column number number
     */
    public int getAimColnumValue(){
        return this.aimcolnum;
    }
    
    /**
     * Set aim column number value
     * @param value aim column number value
     */
    public void setAimColnumValue(int value){
        this.aimcolnum = value;
    }
    
    /**
     * Get time delta type
     * @return Time delta type
     */
    public String getTimeDeltaType(){
        return this.timeDeltaType;
    }
    
    /**
     * Set time delta type
     * @param value Time delta type
     */
    public void setTimeDeltaType(String value){
        this.timeDeltaType = value;
    }
    // </editor-fold>
    // <editor-fold desc="Methods">
    /**
     * Export to xml document
     *
     * @param doc xml document
     * @param parent Parent xml element
     */
    public void exportToXML(Document doc, Element parent) {
        Element root = doc.createElement("DataSetting");
        Attr dataColNamesAttr = doc.createAttribute("DataColumnNames");
        Attr timeDataAttr = doc.createAttribute("TimeData");
        Attr timeColNameAttr = doc.createAttribute("TimeColumnName");
        Attr dateFormatAttr = doc.createAttribute("DateFormat");
        Attr timeDeltaValueAttr = doc.createAttribute("TimeDeltaValue");
        Attr timeDeltaTypeAttr = doc.createAttribute("TimeDeltaType");
        
        String colNames = "";
        if (this.dataColumnNames.size() > 0){
            colNames = this.dataColumnNames.get(0);
            for (int i = 1; i < this.dataColumnNames.size(); i++)
                colNames = colNames + "," + this.dataColumnNames.get(i);
        }
        dataColNamesAttr.setValue(colNames);
        timeDataAttr.setValue(String.valueOf(this.timeData));
        timeColNameAttr.setValue(this.timeColumnName);
        dateFormatAttr.setValue(this.dateFormat);
        timeDeltaValueAttr.setValue(String.valueOf(this.timeDeltaValue));
        timeDeltaTypeAttr.setValue(this.timeDeltaType);
        
        root.setAttributeNode(dataColNamesAttr);
        root.setAttributeNode(timeDataAttr);
        root.setAttributeNode(timeColNameAttr);
        root.setAttributeNode(dateFormatAttr);
        root.setAttributeNode(timeDeltaValueAttr);
        root.setAttributeNode(timeDeltaTypeAttr);
        
        parent.appendChild(root);
    }
    
    /**
     * Import legend scheme from xml node
     *
     * @param flagNode xml node
     */
    public void importFromXML(Node flagNode) {
        try {
            String colNameStr = flagNode.getAttributes().getNamedItem("DataColumnNames").getNodeValue();
            this.dataColumnNames.clear();
            if (!colNameStr.isEmpty()){
                String[] colNames = colNameStr.split(",");
                this.dataColumnNames.addAll(Arrays.asList(colNames));
            }
            this.timeData = Boolean.parseBoolean(flagNode.getAttributes().getNamedItem("TimeData").getNodeValue());
            this.timeColumnName = flagNode.getAttributes().getNamedItem("TimeColumnName").getNodeValue();
            this.dateFormat = flagNode.getAttributes().getNamedItem("DateFormat").getNodeValue();
            this.timeDeltaValue = Integer.parseInt(flagNode.getAttributes().getNamedItem("TimeDeltaValue").getNodeValue());
            this.timeDeltaType = flagNode.getAttributes().getNamedItem("TimeDeltaType").getNodeValue();           
        } catch (Exception e){
            
        }
    }
    // </editor-fold>
}
