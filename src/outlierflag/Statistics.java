/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package outlierflag;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author huangshuai
 */
public class Statistics {
     /// <summary>
    /// Quantile
    /// </summary>
    /// <param name="aDataList">data list</param>
    /// <param name="qValue">quantile index</param>
    /// <returns>quantile value</returns>
    public static double quantile(List<Double> aDataList, double qValue) {
        Collections.sort(aDataList);
        double aData;

        if (qValue == 0) {
            aData = aDataList.get(0);
        } else if (qValue == 1) {
            aData = aDataList.get(aDataList.size() - 1);
        } else {
            aData = aDataList.get((int) (aDataList.size() * qValue) - 1);
        }

        return aData;
    }

    public static double StandardDeviation(List<Double> aDataList) {
        double theMean, theSqDev, theSumSqDev, theVariance, theStdDev, theValue;
        int i;

        theMean = mean(aDataList);
        theSumSqDev = 0;
        for (i = 0; i < aDataList.size(); i++) {
            theValue = aDataList.get(i);
            theSqDev = (theValue - theMean) * (theValue - theMean);
            theSumSqDev = theSqDev + theSumSqDev;
        }

        if (aDataList.size() > 1) {
            theVariance = theSumSqDev / (aDataList.size() - 1);
            theStdDev = Math.sqrt(theVariance);
        } else {
            theVariance = 0;
            theStdDev = 0;
        }

        return theStdDev;
    }

    public static double mean(List<Double> aDataList) {
        double aSum = 0.0;

        for (int i = 0; i < aDataList.size(); i++) {
            aSum = aSum + aDataList.get(i);
        }

        return aSum / aDataList.size();
    }

    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("^(-)?\\d+(\\.\\d+)?$");    
        //Pattern pattern = Pattern.compile("-?[0-9]+.?[0-9]+");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }
}


