/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package outlierflag;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author huangshuai
 */
public class FileUtil {
    /**
     * Read data line list from a file
     * @param fileName File name
     * @return Data lines
     */
    public static List<String> readFileByLines(String fileName) {
        File file = new File(fileName);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            List<String> dataList = new ArrayList<>();            
            String line;
            while ((line = br.readLine()) != null) {
                dataList.add(line);                
            }
            br.close();
            return dataList;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e1) {
                }
            }
        }
    }
    
    /**
     * Read data to DefaultTableModel from a file
     * @param file File name
     * @param separator Line separator
     * @param titleLine If has title line
     * @param headLines Head line number
     * @return Result DefaultTableModel
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static DefaultTableModel readData(String file, String separator, boolean titleLine, int headLines) 
            throws FileNotFoundException, IOException {        
        //Read data
        List<String> dataList = FileUtil.readFileByLines(file);        
        String firstLine = dataList.get(headLines);
        String[] dataArray = firstLine.split(separator);
        int colNum = dataArray.length;
        int rowNum = dataList.size() - headLines;
        if (titleLine)
            rowNum -= 1;
        
        //Get column names
        int i;
        Object[] colNames = new Object[colNum];
        if (titleLine){
            for (i = 0; i < colNum; i++)
                colNames[i] = dataArray[i];
        } else {
            for (i = 0; i < colNum; i++)
                colNames[i] = "C_" + String.valueOf(i + 1);
        }
        
        //Get data
        Object[][] data = new Object[rowNum][colNum];
        int si = headLines;
        String line;
        if (titleLine)
            si += 1;
        for (i = si; i < dataList.size(); i++){
            line = dataList.get(i);
            dataArray = line.split(separator);
            for (int j = 0; j < dataArray.length; j++){
                data[i - si][j] = dataArray[j];
            }
        }
        
        //Get DefaultTableModel
        DefaultTableModel tableModel = new DefaultTableModel(data, colNames);
        
        return tableModel;
    }

}
 

