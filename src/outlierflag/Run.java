package outlierflag;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import org.meteoinfo.chart.Chart;
import org.meteoinfo.chart.plot.ChartPlotMethod;
import org.meteoinfo.chart.plot.SeriesLegend;
import org.meteoinfo.chart.plot.XY1DPlot;
import org.meteoinfo.data.XYListDataset;
import org.meteoinfo.drawing.PointStyle;
import org.meteoinfo.global.util.DateUtil;
import org.meteoinfo.legend.PointBreak;
import ucar.ma2.Array;

/**
 *
 * @author huangshuai
 */
public class Run {

    int styear, stday, sthour, stminute;
    int endyear, endday, endhour, endminute;    
    
    public static String check_1(String infn, String outfn, String separate, String[] colnum, String datecolnum,
            String dateformat, FlagSetting asetting, boolean dt, int ccount)
            throws IOException, Exception {
        BufferedReader br = new BufferedReader(new FileReader(new File(infn)));
        String line = br.readLine();
        String[] outdata = line.split(separate);

        int[] colnums = new int[colnum.length];
        for (int i = 0; i < colnum.length; i++) {
            colnums[i] = Integer.parseInt(colnum[i]);
        }
        //TableData tsData = new TableData();
        //tsData.readASCIIFile(outfn);

        List<String> dataList = FileUtil.readFileByLines(infn);

        Calendar cal = Calendar.getInstance();
        List<Date> dateList = new ArrayList<>();
        Date date;
        SimpleDateFormat format = new SimpleDateFormat(dateformat);
        //int year, month, dayofmonth, dayofweek, dayofwm, dayOfYear, hour, min, second;
        int datecol;
        List<List<String>> outdatalist1 = new ArrayList<>();
        List<List<String>> outfndatalist1 = new ArrayList<>();
        List<List<String>> codeList1 = new ArrayList<>();
        List<String> firstcolumn = new ArrayList();
        for (int i = 0; i < dataList.size(); i++) {
            String[] data = dataList.get(i).split(separate);
            datecol = Integer.parseInt(datecolnum);
            String datedata = data[datecol - 1];
            date = format.parse(datedata);
            dateList.add(date);
        }

        for (int i = 0; i < dataList.size(); i++) {
            String[] data = dataList.get(i).split(separate);
            String fcolumn = data[0];
            firstcolumn.add(fcolumn);
        }

        for (int q = 1; q < outdata.length; q++) {
            List<String> out = new ArrayList<>();
            for (int i = 0; i < dataList.size(); i++) {
                String[] data = dataList.get(i).split(separate);

                try {
                    out.add(data[q]);
                } catch (Exception e) {
                    //System.out.println(q);
                }
            }
            outfndatalist1.add(out);
        }
        for (int p = 0; p < colnum.length; p++) {
            List<String> outdatalist = new ArrayList<>();
            for (int i = 0; i < dataList.size(); i++) {
                String[] data = dataList.get(i).split(separate);
                try {
                    outdatalist.add(data[colnums[p] - 1]);
                } catch (Exception e) {

                }
            }
            outdatalist1.add(outdatalist);

            List<String> codeList = FlagCheck.check_Limitation(outdatalist, asetting);
            FlagCheck.check_Error(outdatalist, asetting, codeList);
            FlagCheck.check_StandardDeviation(outdatalist, asetting, codeList);
            codeList1.add(codeList);
        }

        File dest = new File(outfn);
        FileWriter writer = new FileWriter(dest);
//        int o = 1;
//        writer.write(o);
//        writer.close();
        String lineout = "";

        try {
            for (int i = 0; i < dataList.size(); i++) {

                lineout = firstcolumn.get(i);
                for (List<String> dl : outfndatalist1) {
                    lineout = lineout + separate + dl.get(i);
                    //System.out.println(lineout);
                }
                for (List<String> dl : outdatalist1) {
                    lineout = lineout + separate + dl.get(i);
                }
                for (List<String> cl : codeList1) {
                    lineout = lineout + separate + cl.get(i);
                }

                writer.write(lineout + "\n");
                //System.out.println(lineout);
            }
            writer.close();
        } catch (Exception e) {

        }
        return lineout;
    }
    
    /**
     * Outlier flag function
     * @param inData Input data
     * @param colnums Column indices
     * @param asetting Flag setting
     * @return Flag code list
     * @throws IOException
     * @throws Exception 
     */
    public static List<List<String>> flag(DefaultTableModel inData, List<Integer> colnums, FlagSetting asetting)
            throws IOException, Exception {
        
        int rowNum = inData.getRowCount();
        List<List<String>> codeList1 = new ArrayList<>();  
        List<String> colNames = new ArrayList<>();
        String vstr, colName;
        Object v;
        for (int p : colnums) {
            List<String> outdatalist = new ArrayList<>();
            for (int i = 0; i < rowNum; i++) {
                v = inData.getValueAt(i, p);
                if (v == null)
                    vstr = "";
                else
                    vstr = inData.getValueAt(i, p).toString();
                outdatalist.add(vstr);
            }

            List<String> codeList = FlagCheck.check_Limitation(outdatalist, asetting);
            FlagCheck.check_Error(outdatalist, asetting, codeList);
            FlagCheck.check_StandardDeviation(outdatalist, asetting, codeList);
            codeList1.add(codeList);
            colName = inData.getColumnName(p);
            colNames.add("Flag_" + colName);
        }
        codeList1.add(0, colNames);

        return codeList1;
    }

    public void ReadFF(String infn, String file, String separate) throws FileNotFoundException, IOException {

        List<String> dataList = FileUtil.readFileByLines(infn);
        List<String> dList = new ArrayList<>();
        for (int i = 1; i < dataList.size(); i++) {
            String[] data = dataList.get(i).split(separate);
            dList.add(data[1]);
        }
        RandomAccessFile raf = new RandomAccessFile(file, "r");
        long len = raf.length();
        String lastLine = "";
        if (len != 0L) {
            long pos = len - 1;
            while (pos > 0) {
                pos--;
                raf.seek(pos);
                if (raf.readByte() == '\n') {
                    lastLine = raf.readLine();
                    break;
                }
            }
        }
        String[] date = lastLine.split(separate);
        this.endyear = Integer.parseInt(date[2]);
        this.endday = Integer.parseInt(date[3]);
        this.endhour = Integer.parseInt(date[4].substring(0, 2));
        this.endminute = Integer.parseInt(date[4].substring(2));
        raf.close();
        BufferedReader br = new BufferedReader(new FileReader(new File(file)));
        String line = br.readLine();
        date = line.split(separate);
        this.styear = Integer.parseInt(date[2]);
        this.stday = Integer.parseInt(date[3]);
        this.sthour = Integer.parseInt(date[4].substring(0, 2));
        this.stminute = Integer.parseInt(date[4].substring(2));
        br.close();
    }

    /**
     * Get date list - String
     * @param stdate Start date
     * @param enddate End date
     * @param dateformat Date format string
     * @param tdtype Calendar type
     * @param timeDelt Time delta value
     * @return Date list
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParseException 
     */
    public static List<String> getDateList(String stdate, String enddate, String dateformat, String tdtype, int timeDelt)
            throws FileNotFoundException, IOException, ParseException {
        Calendar stCal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat(dateformat);
        Date stdt = format.parse(stdate);
        stCal.setTime(stdt);
        Calendar endCal = Calendar.getInstance();
        Date enddt = format.parse(enddate);
        endCal.setTime(enddt);
        //stCal.add(Calendar.HOUR_OF_DAY, 8);
        //endCal.add(Calendar.HOUR_OF_DAY, 8);
        List<String> dates = new ArrayList<>();        

        while (stCal.before(endCal)) {
            dates.add(format.format(stCal.getTime()));
            switch (tdtype) {
                case "YEAR":
                    stCal.add(Calendar.YEAR, timeDelt);
                    break;
                case "MONTH":
                    stCal.add(Calendar.MONTH, timeDelt);
                    break;
                case "DAY_OF_MONTH":
                    stCal.add(Calendar.DAY_OF_MONTH, timeDelt);
                    break;
                case "DAY_OF_WEEK":
                    stCal.add(Calendar.DAY_OF_WEEK, timeDelt);
                    break;
                case "DAY_OF_WEEK_IN_MONTH":
                    stCal.add(Calendar.DAY_OF_WEEK_IN_MONTH, timeDelt);
                    break;
                case "DAY_OF_YEAR":
                    stCal.add(Calendar.DAY_OF_YEAR, timeDelt);
                    break;
                case "HOUR":
                    stCal.add(Calendar.HOUR, timeDelt);
                    break;
                case "MINUTE":
                    stCal.add(Calendar.MINUTE, timeDelt);
                    break;
                default:
                    stCal.add(Calendar.SECOND, timeDelt);
                    break;
            }            
        }
        dates.add(format.format(endCal.getTime()));
//        System.out.println(format.format(endCal.getTime()));
        return dates;
    }
    
    /**
     * Time order for data
     * @param inData Input data
     * @param dateColIdx Date column index
     * @param stdate Start date
     * @param enddate End date
     * @param dateformat Date format string
     * @param tdtype Calendar type
     * @param timeDelt Time delta
     * @return Ordered data
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ParseException 
     */
    public static Object[][] timeOrder(Vector inData, int dateColIdx, String stdate, String enddate, String dateformat,
            String tdtype, int timeDelt) throws IOException, FileNotFoundException, ParseException{
        List<String> dateList = getDateList(stdate, enddate, dateformat, tdtype, timeDelt);
        int lineNum = inData.size();
        int colNum = ((Vector)inData.elementAt(0)).size();
        int rowNum = dateList.size();
        Object[][] outData = new Object[rowNum][colNum];
        for (int i = 0; i < rowNum; i++)
            outData[i][0] = dateList.get(i);
        
        int idx;
        String dateStr;        
        for (int i = 0; i < lineNum; i++) {
            dateStr = ((Vector)inData.elementAt(i)).elementAt(dateColIdx).toString();
            idx = dateList.indexOf(dateStr);
            if (idx >= 0) {
                for (int j = 1; j < colNum; j++)
                    outData[idx][j] = ((Vector)inData.elementAt(i)).elementAt(j).toString();
            }
        }
        
        return outData;
    }

    public static String cheatFile(String infn, String outfn, String stdate, String enddate, String dateformat,
            String separate, String tdtype, int timeDelt, String context, String datecolnum, boolean dt, int jTcolnum)
            throws IOException, ParseException {

        List<String> dataList = FileUtil.readFileByLines(outfn);
        List<String> outdataList = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        List<String> dateList = getDateList(stdate, enddate, dateformat, tdtype, timeDelt);
        for (String d : dateList) {
            outdataList.add("");
        }
        Date date;
        SimpleDateFormat format = new SimpleDateFormat(dateformat);
        //int year, month, dayofmonth, dayofweek, dayofwm, dayOfYear, hour, min, second;
        int datecol;
        int idx;
        String line, dateStr;
        for (int i = 0; i < dataList.size(); i++) {
            String[] data = dataList.get(i).split(separate);
            datecol = Integer.parseInt(datecolnum);
            String datedata = data[datecol - 1];
            date = format.parse(datedata);
            dateStr = format.format(date);
            idx = dateList.indexOf(dateStr);
            if (idx >= 0) {
                line = dataList.get(i);
                outdataList.set(idx, line);
            }
        }

        File dest = new File(outfn);
        FileWriter writer = new FileWriter(dest);
        for (int i = 0; i < outdataList.size(); i++) {
            line = dateList.get(i);
            if (outdataList.get(i).isEmpty()) {
                for (int j = 0; j < jTcolnum; j++) {
                    line = line + ",";
                }
            } else {
                line = line + "," + outdataList.get(i);
            }
            writer.write(line + "\n");
//            System.out.println(line);
        }
        writer.close();
        return outfn;
    }

    public static Chart plot(String outfn, String separate, String dateformat, String[] colnum) throws ParseException, IOException, Exception {

        BufferedReader br = new BufferedReader(new FileReader(new File(outfn)));
        String line = br.readLine();
        String[] outfndata = line.split(separate);
        int o = outfndata.length;

        List<List<Number>> outdata1 = new ArrayList<>();
        List<List<String>> codeList1 = new ArrayList<>();
        List<String> dataList = FileUtil.readFileByLines(outfn);

        int l = dataList.size();
        int c = colnum.length;
        List<Number> outdata = new ArrayList<>();
        List<Number> xdata = new ArrayList<>();
        List<String> codeList = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat(dateformat);

        int i, j;
        XYListDataset dataset = new XYListDataset();
        //for (j = 0; j < c; j++){

        for (i = 0; i < l; i++) {
            String[] data = dataList.get(i).split(separate);
            try {
                outdata.add(Double.parseDouble(data[o - 2 * c]));
                xdata.add(DateUtil.toOADate(format.parse(data[0])));
                //xdata.add(1.0 * i);
                codeList.add(data[o - c].trim());
            } catch (Exception e) {

            }
            //System.out.println(data[1]);
        }

        dataset.addSeries("S_" + String.valueOf(0), xdata, outdata);
        xdata.clear();
        outdata.clear();
        //}

        //XYDataset dataset = new XYArrayDataset(xdata, outdata, "Test");        
        XY1DPlot plot = new XY1DPlot(true, dataset);
        plot.setChartPlotMethod(ChartPlotMethod.POINT);
        plot.setTitle("Test");
        plot.getGridLine().setDrawXLine(true);
        plot.getGridLine().setDrawYLine(true);
        SeriesLegend slegend = new SeriesLegend(dataset.getItemCount());
        plot.setLegendBreak(0, slegend);
        PointBreak pb1 = new PointBreak();
        pb1.setStyle(PointStyle.Plus);
        pb1.setDrawOutline(false);
        pb1.setColor(Color.red);
        PointBreak pb2 = new PointBreak();
        pb2.setStyle(PointStyle.DownTriangle);
        pb2.setDrawOutline(false);
        pb2.setColor(Color.green);
        pb2.setSize(8);
        i = 0;
        for (String code : codeList) {
            if (code.equals("V0")) {
                plot.setItemPointBreak(0, i, pb1);
            } else {
                plot.setItemPointBreak(0, i, pb2);
            }
            i += 1;
        }

        plot.setUseBeak2D(true);
        Chart chart = new Chart(plot);
//        JFrame frmChart = new JFrame();
//        frmChart.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        frmChart.setSize(800, 500);
//        frmChart.getContentPane().add(chartPanel, BorderLayout.CENTER);
//        frmChart.setVisible(true);
        
//        FrmChart frmChart = new FrmChart(null);
//        frmChart.setChart(chart);
//        frmChart.setSize(800, 500);
//        frmChart.setVisible(true);
        return chart;
    }
    
    public static Chart plot(String colName, List<Number> dataList, List<String> codeList) 
            throws ParseException, IOException, Exception {
        int i;
        XYListDataset dataset = new XYListDataset();   
        List<Number> xdata = new ArrayList<>();
        for (i = 0; i < dataList.size(); i++){
            xdata.add(i);
        }
        dataset.addSeries(colName, xdata, dataList);
        dataset.setMissingValue(-9999.0);
        
        XY1DPlot plot = new XY1DPlot(false, dataset);        
        plot.setChartPlotMethod(ChartPlotMethod.POINT);
        plot.setTitle(colName);
        plot.getGridLine().setDrawXLine(true);
        plot.getGridLine().setDrawYLine(true);
        SeriesLegend slegend = new SeriesLegend(dataset.getItemCount());
        plot.setLegendBreak(0, slegend);
        PointBreak pb1 = new PointBreak();
        pb1.setStyle(PointStyle.Plus);
        pb1.setDrawOutline(false);
        pb1.setColor(Color.red);
        PointBreak pb2 = new PointBreak();
        pb2.setStyle(PointStyle.DownTriangle);
        pb2.setDrawOutline(false);
        pb2.setColor(Color.green);
        pb2.setSize(8);
        PointBreak pb3 = new PointBreak();
        pb3.setDrawOutline(false);
        pb3.setStyle(PointStyle.Square);
        pb3.setColor(Color.blue);
        i = 0;
        for (String code : codeList) {
            switch (code){
                case "V0":
                    plot.setItemPointBreak(0, i, pb1);
                    break;
                case "VU":
                    plot.setItemPointBreak(0, i, pb3);
                    break;
                default:
                    plot.setItemPointBreak(0, i, pb2);
                    break;
            }
            i += 1;
        }

        plot.setUseBeak2D(true);        
        Chart chart = new Chart(plot); 
        return chart;
    }
    
    public static Chart plot(String colName, List<Number> dataList, List<Number> dateList, List<String> codeList) 
            throws ParseException, IOException, Exception {
        int i;
        XYListDataset dataset = new XYListDataset();        
        dataset.addSeries(colName, dateList, dataList);
        dataset.setMissingValue(-9999.0);
        
        XY1DPlot plot = new XY1DPlot(true, dataset);        
        plot.setChartPlotMethod(ChartPlotMethod.POINT);
        plot.setTitle(colName);
        plot.getGridLine().setDrawXLine(true);
        plot.getGridLine().setDrawYLine(true);
        SeriesLegend slegend = new SeriesLegend(dataset.getItemCount());
        plot.setLegendBreak(0, slegend);
        PointBreak pb1 = new PointBreak();
        pb1.setStyle(PointStyle.Plus);
        pb1.setDrawOutline(false);
        pb1.setColor(Color.red);
        PointBreak pb2 = new PointBreak();
        pb2.setStyle(PointStyle.DownTriangle);
        pb2.setDrawOutline(false);
        pb2.setColor(Color.green);
        pb2.setSize(8);
        PointBreak pb3 = new PointBreak();
        pb3.setDrawOutline(false);
        pb3.setStyle(PointStyle.Square);
        pb3.setColor(Color.blue);
        i = 0;
        for (String code : codeList) {
            switch (code){
                case "V0":
                    plot.setItemPointBreak(0, i, pb1);
                    break;
                case "VU":
                    plot.setItemPointBreak(0, i, pb3);
                    break;
                default:
                    plot.setItemPointBreak(0, i, pb2);
                    break;
            }
            i += 1;
        }

        plot.setUseBeak2D(true);        
        Chart chart = new Chart(plot);        
//        JFrame frmChart = new JFrame();
//        frmChart.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        frmChart.setSize(800, 500);
//        frmChart.getContentPane().add(chartPanel, BorderLayout.CENTER);
//        frmChart.setVisible(true);
        
//        FrmChart frmChart = new FrmChart(null);
//        frmChart.setChart(chart);
//        frmChart.setSize(800, 500);
//        frmChart.setVisible(true);
        return chart;
    }
    
    /**
     * Plot data
     * @param colName
     * @param data
     * @param dateList
     * @param codeList
     * @return Chart
     * @throws ParseException
     * @throws IOException
     * @throws Exception 
     */
    public static Chart plot(String colName, Array data, List<Number> dateList, List<String> codeList) 
            throws ParseException, IOException, Exception {
        int i;
        XYListDataset dataset = new XYListDataset();        
        dataset.addSeries(colName, dateList, data);
        dataset.setMissingValue(-9999.0);
        
        XY1DPlot plot = new XY1DPlot(true, dataset);        
        plot.setChartPlotMethod(ChartPlotMethod.POINT);
        plot.setTitle(colName);
        plot.getGridLine().setDrawXLine(true);
        plot.getGridLine().setDrawYLine(true);
        SeriesLegend slegend = new SeriesLegend(dataset.getItemCount());
        plot.setLegendBreak(0, slegend);
        PointBreak pb1 = new PointBreak();
        pb1.setStyle(PointStyle.Plus);
        pb1.setDrawOutline(false);
        pb1.setColor(Color.red);
        PointBreak pb2 = new PointBreak();
        pb2.setStyle(PointStyle.DownTriangle);
        pb2.setDrawOutline(false);
        pb2.setColor(Color.green);
        pb2.setSize(8);
        PointBreak pb3 = new PointBreak();
        pb3.setDrawOutline(false);
        pb3.setStyle(PointStyle.Square);
        pb3.setColor(Color.blue);
        i = 0;
        for (String code : codeList) {
            switch (code){
                case "V0":
                    plot.setItemPointBreak(0, i, pb1);
                    break;
                case "VU":
                    plot.setItemPointBreak(0, i, pb3);
                    break;
                default:
                    plot.setItemPointBreak(0, i, pb2);
                    break;
            }
            i += 1;
        }

        plot.setUseBeak2D(true);        
        Chart chart = new Chart(plot);                
        return chart;
    }
    
     /**
     * Plot data
     * @param colName
     * @param data
     * @param codeList
     * @return Chart
     * @throws ParseException
     * @throws IOException
     * @throws Exception 
     */
    public static Chart plot(String colName, Array data, List<String> codeList) 
            throws ParseException, IOException, Exception {
        int i;
        XYListDataset dataset = new XYListDataset();   
        List<Number> xdata = new ArrayList<>();
        for (i = 0; i < data.getSize(); i++){
            xdata.add(i);
        }
        dataset.addSeries(colName, xdata, data);
        dataset.setMissingValue(-9999.0);
        
        XY1DPlot plot = new XY1DPlot(false, dataset);        
        plot.setChartPlotMethod(ChartPlotMethod.POINT);
        plot.setTitle(colName);
        plot.getGridLine().setDrawXLine(true);
        plot.getGridLine().setDrawYLine(true);
        SeriesLegend slegend = new SeriesLegend(dataset.getItemCount());
        plot.setLegendBreak(0, slegend);
        PointBreak pb1 = new PointBreak();
        pb1.setStyle(PointStyle.Plus);
        pb1.setDrawOutline(false);
        pb1.setColor(Color.red);
        PointBreak pb2 = new PointBreak();
        pb2.setStyle(PointStyle.DownTriangle);
        pb2.setDrawOutline(false);
        pb2.setColor(Color.green);
        pb2.setSize(8);
        PointBreak pb3 = new PointBreak();
        pb3.setDrawOutline(false);
        pb3.setStyle(PointStyle.Square);
        pb3.setColor(Color.blue);
        i = 0;
        for (String code : codeList) {
            switch (code){
                case "V0":
                    plot.setItemPointBreak(0, i, pb1);
                    break;
                case "VU":
                    plot.setItemPointBreak(0, i, pb3);
                    break;
                default:
                    plot.setItemPointBreak(0, i, pb2);
                    break;
            }
            i += 1;
        }

        plot.setUseBeak2D(true);        
        Chart chart = new Chart(plot); 
        return chart;
    }
}
