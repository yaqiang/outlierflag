/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outlierflag;

import javax.swing.UIManager;
import javax.swing.WindowConstants;
import outlierflag.gui.FrmMain;

/**
 *
 * @author huangshuai
 */
public class Main {

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        runGUI(); 
    }
    
    private static void runGUI(){
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {                
                FrmMain frame = new FrmMain();
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setSize(800, 600);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);                
            }
        });
    }
        
}
