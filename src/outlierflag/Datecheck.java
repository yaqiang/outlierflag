/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outlierflag;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.UIManager;

/**
 *
 * @author huangshuai
 */
public class Datecheck {

    public static String datecheck(String infn, String outfn) throws IOException, Exception {
        List<String> dataList = FileUtil.readFileByLines(infn);
        Calendar cal = Calendar.getInstance();
        List<Date> dateList = new ArrayList<>();
        Date date;
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
        int year, dayOfYear, hour, min;

        for (int i = 0; i < dataList.size(); i++) {
            String[] data = dataList.get(i).split(",");           
            year = Integer.parseInt(data[0]);
            dayOfYear = Integer.parseInt(data[1]);
            Float h = Float.parseFloat(data[2]);
            Float h1 = h/100;
            int hh = (int)Math.floor(h1);
           
            int mm = (int) Math.floor(h%100);
            hour = hh;
            min = mm;
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.DAY_OF_YEAR, dayOfYear);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, min);
            cal.add(Calendar.HOUR, 8);
            date = cal.getTime();
            dateList.add(date);

        }

        
        File dest = new File(outfn);
        FileWriter writer = new FileWriter(dest);
        String line = null;
        for (int i = 0; i < dataList.size(); i++) {
            line = format.format(dateList.get(i));
            line = line + "," + dataList.get(i);
            writer.write(line + "\n");
            
        }
        writer.close();
        return line;
    }
    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        String infn = "E:\\study\\Taishan_Data\\4 ASP_S\\54826ASP201006.txt";
        String outfn = "F:\\result\\ASP\\datecheck\\54826ASP201006.txt";
        Datecheck.datecheck(infn, outfn); 
    }
}
