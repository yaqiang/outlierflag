/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outlierflag.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import org.meteoinfo.global.GenericFileFilter;
import outlierflag.DataSetting;

/**
 *
 * @author huangshuai
 */
public class FrmAvg extends javax.swing.JFrame {

    private FrmMain parent;
    private File houravgFile;
    private File dayavgFile;

    /**
     * Creates new form FrmAvg
     *
     * @param parent
     */
    public FrmAvg(FrmMain parent) {
        initComponents();

        this.parent = parent;
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    public void setParent(FrmMain parent) {
        this.parent = parent;
    }

    /**
     * Get hourly standard date list
     *
     * @param stdate Start date
     * @param enddate End date
     * @param dateformat Date format
     * @return Hourly date list
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParseException
     */
    public List<String> getDateList_Hour(String stdate, String enddate, String dateformat)
            throws FileNotFoundException, IOException, ParseException {

        int idx = 0;
        if (dateformat.contains("H")) {
            idx = dateformat.indexOf("H") + 2;
        } else if (dateformat.contains("h")) {
            idx = dateformat.indexOf("h") + 2;
        } else if (dateformat.contains("K")) {
            idx = dateformat.indexOf("K") + 2;
        } else if (dateformat.contains("k")) {
            idx = dateformat.indexOf("k") + 2;
        }

        String newdateformat = dateformat.substring(0, idx);
        SimpleDateFormat format = new SimpleDateFormat(newdateformat);
        Calendar stCal = Calendar.getInstance();
        String stdate1 = stdate.substring(0, idx);
        Date stdt = format.parse(stdate1);
        stCal.setTime(stdt);
        Calendar endCal = Calendar.getInstance();
        String enddate1 = enddate.substring(0, idx);
        Date enddt = format.parse(enddate1);
        endCal.setTime(enddt);
        List<String> dates = new ArrayList<>();

        while (stCal.before(endCal)) {
            dates.add(format.format(stCal.getTime()));
            stCal.add(Calendar.HOUR, 1);
        }
        dates.add(format.format(endCal.getTime()));
        return dates;
    }

    /**
     * Get daily standard date list
     *
     * @param stdate Start date
     * @param enddate End date
     * @param dateformat Date format
     * @return Daily date list
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParseException
     */
    public List<String> getDateList_Day(String stdate, String enddate, String dateformat)
            throws FileNotFoundException, IOException, ParseException {
        String day = null;
        int idx = 0;

        if (dateformat.contains("D")) {
            idx = dateformat.indexOf("D") + 2;
        } else if (dateformat.contains("d")) {
            idx = dateformat.indexOf("d") + 2;
        }

        String newdateformat = dateformat.substring(0, idx);
        SimpleDateFormat format = new SimpleDateFormat(newdateformat);
        Calendar stCal = Calendar.getInstance();
        String stdate1 = stdate.substring(0, idx);
        Date stdt = format.parse(stdate1);
        stCal.setTime(stdt);
        Calendar endCal = Calendar.getInstance();
        String enddate1 = enddate.substring(0, idx);
        Date enddt = format.parse(enddate1);
        endCal.setTime(enddt);
        List<String> dates = new ArrayList<>();
        while (stCal.before(endCal)) {
            dates.add(format.format(stCal.getTime()));
            if (dateformat.contains("D")) {
                stCal.add(Calendar.DAY_OF_YEAR, 1);
            } else if (dateformat.contains("d")) {
                stCal.add(Calendar.DAY_OF_MONTH, 1);
            }
        }
        dates.add(format.format(endCal.getTime()));
        return dates;
    }

    /**
     * Calculate hourly or daily average value
     *
     * @param avgFile Output file
     * @param datelist Standard date list
     * @param headline
     * @return Average value
     * @throws ParseException
     * @throws IOException
     * @throws Exception
     */
    public List<List<String>> AVG(File avgFile, List<String> datelist, String headline) throws ParseException, IOException, Exception {
        DefaultTableModel tableModel = (DefaultTableModel) this.parent.getTable().getModel();
        DataSetting ds = this.parent.getDataSetting();
        int rowcolnum = tableModel.getRowCount();
        int aimcolnum = ds.getDataColumnNames().size();
        List<String> datacolnames = ds.getDataColumnNames();
        int length = datelist.size();

        List<List<String>> AVG = new ArrayList<>();
        try {
            for (int col = 0; col < aimcolnum; col++) {
                String dataColName = datacolnames.get(col);
                int dataColIdx = tableModel.findColumn(dataColName);
                int codeColIdx = tableModel.findColumn("Flag_" + dataColName);
                List<String> Avg = new ArrayList<>();
                for (int i = 0; i < length; i++) {
                    String datehour = datelist.get(i);
                    float sum = 0;
                    int size = 0;
                    for (int j = 0; j < rowcolnum; j++) {
                        String datetable = (String) tableModel.getValueAt(j, 0);
                        String datetable1 = datetable.substring(0, datehour.length());
                        String codetable = (String) tableModel.getValueAt(j, codeColIdx);
                        if (datetable1.equals(datehour) && codetable.equals("V0")) {
                            String datatable = (String) tableModel.getValueAt(j, dataColIdx);
                            Float datatable1 = Float.parseFloat(datatable);
                            sum += datatable1;
                            size++;
                        } else {
                            continue;
                        }
                    }
                    System.out.println(sum);
                    System.out.println(size);
                    float result = sum / size;
                    String result1 = "" + result;
                    Avg.add(result1);
                }
                AVG.add(Avg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        FileWriter writer = new FileWriter(avgFile);
        if (!headline.equals(null)) {
            writer.write(headline + "\n");
        }
        String lineout = "";
        String separator = ",";
        try {
            for (int n = 0; n < length; n++) {
                lineout = datelist.get(n);
                for (List<String> avg : AVG) {
                    lineout = lineout + separator + avg.get(n);
                }
                writer.write(lineout + "\n");
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("catch");
        }
        return AVG;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog_finished = new javax.swing.JDialog();
        jLabel3 = new javax.swing.JLabel();
        jButton_calOK = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField_hourAvgFile = new javax.swing.JTextField();
        jButton_hourAvgFile = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextField_headlineAvg = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField_dayAvgFile = new javax.swing.JTextField();
        jButton_dayavgfile = new javax.swing.JButton();
        jButton_OK = new javax.swing.JButton();
        jButton_cancel = new javax.swing.JButton();

        jLabel3.setFont(new java.awt.Font("宋体", 0, 14)); // NOI18N
        jLabel3.setText("Calculation finished !");

        jButton_calOK.setText("OK");
        jButton_calOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_calOKActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialog_finishedLayout = new javax.swing.GroupLayout(jDialog_finished.getContentPane());
        jDialog_finished.getContentPane().setLayout(jDialog_finishedLayout);
        jDialog_finishedLayout.setHorizontalGroup(
            jDialog_finishedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog_finishedLayout.createSequentialGroup()
                .addGroup(jDialog_finishedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog_finishedLayout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addComponent(jLabel3))
                    .addGroup(jDialog_finishedLayout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(jButton_calOK, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(154, Short.MAX_VALUE))
        );
        jDialog_finishedLayout.setVerticalGroup(
            jDialog_finishedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog_finishedLayout.createSequentialGroup()
                .addContainerGap(75, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton_calOK)
                .addGap(26, 26, 26))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Average Calculation");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Calculation"));

        jLabel1.setText("Hour_Average result file");

        jTextField_hourAvgFile.setText("F:\\result\\ASP\\hourAVG\\54826ASP201006.txt");
        jTextField_hourAvgFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_hourAvgFileActionPerformed(evt);
            }
        });

        jButton_hourAvgFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/outlierflag/resources/TSB_Open.Image.png"))); // NOI18N
        jButton_hourAvgFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_hourAvgFileActionPerformed(evt);
            }
        });

        jLabel2.setText("Headline of result file");

        jTextField_headlineAvg.setText("BJ_Time,VIS");
        jTextField_headlineAvg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_headlineAvgActionPerformed(evt);
            }
        });

        jLabel4.setText("Day_Average result file");

        jTextField_dayAvgFile.setText("F:\\result\\ASP\\dayAVG\\54826ASP201006.txt");
        jTextField_dayAvgFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_dayAvgFileActionPerformed(evt);
            }
        });

        jButton_dayavgfile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/outlierflag/resources/TSB_Open.Image.png"))); // NOI18N
        jButton_dayavgfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_dayavgfileActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField_dayAvgFile))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField_hourAvgFile)
                            .addComponent(jTextField_headlineAvg))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton_hourAvgFile)
                    .addComponent(jButton_dayavgfile))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton_hourAvgFile)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextField_hourAvgFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField_dayAvgFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_dayavgfile))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField_headlineAvg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13))
        );

        jButton_OK.setText("OK");
        jButton_OK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_OKActionPerformed(evt);
            }
        });

        jButton_cancel.setText("Cancel");
        jButton_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addComponent(jButton_OK, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addComponent(jButton_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_OK)
                    .addComponent(jButton_cancel))
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_hourAvgFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_hourAvgFileActionPerformed
        // TODO add your handling code here:
        String path = System.getProperty("user.dir");
        File pathDir = new File(path);
        JFileChooser aDlg = new JFileChooser();
        aDlg.setAcceptAllFileFilterUsed(false);
        aDlg.setCurrentDirectory(pathDir);
        String[] fileExts = new String[]{"txt"};
        GenericFileFilter fileFilter = new GenericFileFilter(fileExts, "Supported Formats");
        aDlg.setFileFilter(fileFilter);
        fileFilter = new GenericFileFilter(fileExts, "Text File(*.txt)");
        aDlg.addChoosableFileFilter(fileFilter);
        if (JFileChooser.APPROVE_OPTION == aDlg.showOpenDialog(this)) {
            File file = aDlg.getSelectedFile();
            System.setProperty("user.dir", file.getParent());
            this.jTextField_hourAvgFile.setText(file.getAbsolutePath());
        }
    }//GEN-LAST:event_jButton_hourAvgFileActionPerformed

    private void jTextField_hourAvgFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_hourAvgFileActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_hourAvgFileActionPerformed

    private void jTextField_headlineAvgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_headlineAvgActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_headlineAvgActionPerformed

    private void jButton_OKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_OKActionPerformed
        // TODO add your handling code here:
        DataSetting ds = this.parent.getDataSetting();
        String format = ds.getDateFormat();
        DefaultTableModel tableModel = (DefaultTableModel) this.parent.getTable().getModel();
        int rowcolnum = tableModel.getRowCount();
        String stdate = (String) tableModel.getValueAt(0, 0);
        String enddate = (String) tableModel.getValueAt(rowcolnum - 1, 0);
        String headline = this.jTextField_headlineAvg.getText();
        if (houravgFile == null) {
            houravgFile = new File(this.jTextField_hourAvgFile.getText());
        }
        if (dayavgFile == null) {
            dayavgFile = new File(this.jTextField_dayAvgFile.getText());
        }
        String headlineavg = this.jTextField_headlineAvg.getText();

        try {
            List<String> datelisth = this.getDateList_Hour(stdate, enddate, format);
            this.AVG(houravgFile, datelisth, headline);
            List<String> datelistd = this.getDateList_Day(stdate, enddate, format);
            this.AVG(dayavgFile, datelistd, headline);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.dispose();
        this.jDialog_finished.setSize(450, 150);
        this.jDialog_finished.setLocationRelativeTo(null);
        this.jDialog_finished.setVisible(true);
    }//GEN-LAST:event_jButton_OKActionPerformed

    private void jButton_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_cancelActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton_cancelActionPerformed

    private void jButton_calOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_calOKActionPerformed
        // TODO add your handling code here:
        this.jDialog_finished.dispose();
    }//GEN-LAST:event_jButton_calOKActionPerformed

    private void jButton_dayavgfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_dayavgfileActionPerformed
        // TODO add your handling code here:
        String path = System.getProperty("user.dir");
        File pathDir = new File(path);
        JFileChooser aDlg = new JFileChooser();
        aDlg.setAcceptAllFileFilterUsed(false);
        aDlg.setCurrentDirectory(pathDir);
        String[] fileExts = new String[]{"txt"};
        GenericFileFilter fileFilter = new GenericFileFilter(fileExts, "Supported Formats");
        aDlg.setFileFilter(fileFilter);
        fileFilter = new GenericFileFilter(fileExts, "Text File(*.txt)");
        aDlg.addChoosableFileFilter(fileFilter);
        if (JFileChooser.APPROVE_OPTION == aDlg.showOpenDialog(this)) {
            File file = aDlg.getSelectedFile();
            System.setProperty("user.dir", file.getParent());
            this.jTextField_dayAvgFile.setText(file.getAbsolutePath());
        }
    }//GEN-LAST:event_jButton_dayavgfileActionPerformed

    private void jTextField_dayAvgFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_dayAvgFileActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_dayAvgFileActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmAvg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmAvg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmAvg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmAvg.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FrmAvg(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_OK;
    private javax.swing.JButton jButton_calOK;
    private javax.swing.JButton jButton_cancel;
    private javax.swing.JButton jButton_dayavgfile;
    private javax.swing.JButton jButton_hourAvgFile;
    private javax.swing.JDialog jDialog_finished;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField_dayAvgFile;
    private javax.swing.JTextField jTextField_headlineAvg;
    private javax.swing.JTextField jTextField_hourAvgFile;
    // End of variables declaration//GEN-END:variables
}
